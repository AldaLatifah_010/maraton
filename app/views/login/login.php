<html>
<head>
	<title>LOGIN Virtual Marathon</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url ?>/assets/css/login.css">
</head>
<body>

	<div class="container">
		<form role="form" method="post" enctype="multipart/form-data" action="<?= base_url; ?>/prosesLogin">
			<h3>LOGIN Virtual Marathon</h3><hr>

			<label>Username</label><br>
			<input class="masukan" type="text" name="username"><br><br>

			<label>Password</label><br>
			<input class="masukan" type="password" name="password"><br><br>

			<input class="tombol" type="submit" name="submit" value="LOGIN">
		</form>
	</div>

</body>
</html>