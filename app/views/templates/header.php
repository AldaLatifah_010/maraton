<html>
<head>
	<title>Virtual Maraton</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url ?>/assets/css/style.css">
	<!-- <link rel="stylesheet" type="text/css" href="icon/fa/css/font-awesome.min.css"> -->
	<script type="text/javascript" src="<?= base_url ?>/assets/js/jquery.js"></script>
	<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</head>
<body>

	<div class="container">
		<nav>
			<ul>
				<li><a href="dashboard.php">Virtual Maraton</a></li>
				<div class="right">
					<li><a href="<?= base_url ?>/logout">LOGOUT</a></li>
				</div>
			</ul>
		</nav>
	</div>
	<div class="row">
		<div class="samping">
			<ul>
				<?php if ($data['level']=="Admin" || $data['level']=="Pemilik") { ?>
				<li><a href="<?= base_url ?>/home">Dashboard</a></li><hr>
				<li><a href="<?= base_url ?>/tema">Manajemen Tema</a></li><hr>
				<li><a href="<?= base_url ?>/track">Manajemen Track</a></li><hr>
				<li><a href="<?= base_url ?>/transaksi">Manajemen Transaksi</a></li><hr>
				<li><a href="<?= base_url ?>/progress">Manajemen Progress</a></li><hr>
				<li><a href="<?= base_url ?>/laporan">Manajemen Laporan</a></li><hr>
				<li><a href="<?= base_url ?>/user">Manajemen Peserta</a></li><hr>
				<?php } else{ ?>
				<li><a href="<?= base_url ?>/home">Dashboard</a></li><hr>
				<li><a href="<?= base_url ?>/track">Track</a></li><hr>
				<li><a href="<?= base_url ?>/transaksi">Manajemen Transaksi</a></li><hr>
				<li><a href="<?= base_url ?>/progress">Manajemen Progress</a></li><hr>
				<?php } ?>
			</ul>
		</div>
	