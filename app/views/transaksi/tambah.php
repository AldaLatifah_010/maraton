
		<div class="isi">
			<h1>Tambah Track</h1><hr>

			<div class="form">
				<form role="form" method="post" enctype="multipart/form-data" action="<?= base_url; ?>/simpanTransaksi">
					<input type="hidden" value="<?= $data['id_user']?>" name="id_user">

					<label>Kode Transaksi</label><br>
					<input class="masukan" type="text" name="kode_transaksi" autocomplete="off" ><br><br>

                    <label>Kategori Track</label><br> 
					<select class="seleect" name="id_track[]" multiple="multiple">
                        <option value="#">Pilih</option>
						<?php foreach ($data['track'] as $row) : ?>
						<option value="<?= $row['id'] ?>"><?php echo $row['nama_track']; ?></option>
						<?php endforeach; ?>
					</select><br><br>

                    <label>Foto Upload</label><br>
					<input class="masukan" type="file" name="file"><br><br>

					<input class="tombol" type="submit" name="kirim" value="Tambah">
					
				</form>
			</div>
		</div>

	</div>

</body>
<script>
	$(document).ready(function() {
    $('.seleect').select2();
});
</script>
</html>