
		<div class="isi">
			<h1>Transaksi</h1><hr>

			<h4><a href="tambahTransaksi">Tambah Transaksi +</a></h4>

			<form method="post" action="<?= base_url?>/cariTransaksi">
				<input type="text" class="form-control" placeholder="" name="key" >
				<input class="tombol" type="submit" name="kirim" value="Cari">	
			</form>


			<table border="1">
				<tr>
					<th style="width:7%;">No.</th>
					<th>Kode Transaksi</th>
					<th>Nama Peserta</th>
					<th>Tanggal Transaksi</th>
					<th>Track</th>
                    <th>Bukti Pembayaran</th>
					<th>Status</th>
					<?php if($data['level'] == "Admin" || $data['level'] == "Pemilik" ) :?>
						<th>Aksi</th>
					<?php endif;?>
				</tr>
			<?php $no=1; foreach ($data['transaksi'] as $row) :?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $row['kode_transaksi']; ?></td>
					<td><?php echo $row['nama'];?></td>
                    <td><?php echo $row['tgl_transaksi']; ?></td>
					<td>
						<details>
						<summary>Detail Pembelian</summary>
							<?php 
								$id = $row['id_trans'];
								$data['track'] = $this->model('TransaksiModel')->getDetailTransaksi($id); 
								foreach($data['track'] as $r) :
							?>
							
							Nama Track : <?php echo $r['nama_track']?><br><br>
							<?php endforeach?>
						</details>
					</td>
                    <td>
						<a target="_blank" href="../img/<?php echo $row['file']; ?>">
							<img src="../img/<?php echo $row['file']; ?>" alt="Cinque Terre" width="600" height="400">
						</a>
					</td>
					<td><?php echo $row['status']; ?></td>
					<?php if($data['level'] == "Admin" || $data['level'] == "Pemilik" ) :?>
						<td><a href="editTransaksi/<?=$row['id_trans']?>">Ubah Status</a></td>
					<?php endif;?>
				</tr>
			<?php endforeach; ?>
			</table>
		</div>
	</div>

</body>
</html>