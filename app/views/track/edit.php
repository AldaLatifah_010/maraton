
		<div class="isi">
			<h1>Edit Track</h1><hr>

			<div class="form">
				<form role="form" method="post" enctype="multipart/form-data" action="<?= base_url; ?>/editTrack">
                    <input type="hidden" name="id" value="<?=$data['track']['id']?>">
                    <input type="hidden" name="img_lama" value="<?= $data['track']['img'] ?>">

					<label>Nama Track</label><br>
					<input class="masukan" type="text" name="nama_track" autocomplete="off" value="<?= $data['track']['nama_track']?>"><br><br>
                    

                    <label>Kategori Tema</label><br>
					<select class="seleect" name="id_tema">
                        <option value="#">Pilih</option>
						<?php foreach ($data['tema'] as $row) : ?>
						<option value="<?= $row['id'] ?>" <?php if($row['id'] == $data['track']['id_tema']) {
                         echo "selected";   
                        }?>><?php echo $row['nama_tema']; ?></option>
						<?php endforeach; ?>
					</select><br><br>

                    <label>Harga Track</label><br>
                    <input class="masukan" type="number" name="harga" autocomplete="off" value="<?= $data['track']['harga']?>"><br><br>

                    <label>Jarak Track</label><br>
                    <input class="masukan" type="number" name="jarak" autocomplete="off" value="<?= $data['track']['jarak']?>"><br><br>
				
                    
                    <img src="../../img/<?=$data['track']['img']?>"> <br><br>
                    <label>Foto Track</label><br>
					<input class="masukan" type="file" name="img"><br><br>

					<input class="tombol" type="submit" name="kirim" value="Edit">
					
				</form>
			</div>
		</div>

	</div>

</body>
</html>