
		<div class="isi">

			

			<?php if($data['level'] == "Admin" || $data['level'] == "Pemilik") { ?>
				<h1> Data Track</h1><hr>
				<h4><a href="tambahTrack">Tambah Track +</a></h4>
			<?php }else{ ?>
				<h1> Pilihan Track</h1><hr>
			<?php } ?>

			<?php if($data['level'] == "Admin" || $data['level']== "Pemilik") {?>
			<table border="1">
				<tr>
					<th style="width:7%;">No.</th>
					<th>Nama Track</th>
                    <th>Harga</th>
                    <th>Jarak</th>
                    <th>Jenis Tema</th>
                    <th>Gambar Track</th>
					<th>Aksi</th>
				</tr>
			<?php $no=1; foreach ($data['track'] as $row) :?>
				<tr>
					<td><?php echo $no++; ?></td>
					<td><?php echo $row['nama_track']; ?></td>
                    <td><?php echo $row['harga']; ?></td>
                    <td><?php echo $row['jarak']; ?></td>
                    <td><?php echo $row['nama_tema']; ?></td>
                    <td><img src="../img/<?php echo $row['img']; ?>" ></td>
					<td><a href="hapusTrack/<?=$row['id']?>">Hapus</a>|<a href="trackEdit/<?=$row['id']?>">Edit</a></td>
				</tr>
			<?php endforeach; ?>
			</table>
		<?php }else{?>
			<?php $no=1; foreach ($data['track'] as $row) :?>
				<div class="gallery">
					<a target="_blank" href="../img/<?php echo $row['img']; ?>">
						<img src="../img/<?php echo $row['img']; ?>" alt="Cinque Terre" width="600" height="400">
					</a>
					<div class="desc"><?= $row['nama_track']?> <br> <?= $row['harga']?>|<?= $row['jarak']?> m</div>
				</div>
			<?php endforeach;?>
		<?php } ?>
		</div>
	</div>

</body>
</html>