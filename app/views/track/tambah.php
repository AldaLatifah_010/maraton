
		<div class="isi">
			
				<h1>Tambah Track</h1><hr>
			

			<div class="form">
				<form role="form" method="post" enctype="multipart/form-data" action="<?= base_url; ?>/simpanTrack">

					<label>Nama Track</label><br>
					<input class="masukan" type="text" name="nama_track" autocomplete="off"><br><br>

                    <label>Kategori Tema</label><br>
					<select class="seleect" name="id_tema">
                        <option value="#">Pilih</option>
						<?php foreach ($data['tema'] as $row) : ?>
						<option value="<?= $row['id'] ?>"><?php echo $row['nama_tema']; ?></option>
						<?php endforeach; ?>
					</select><br><br>

                    <label>Harga Track</label><br>
                    <input class="masukan" type="number" name="harga" autocomplete="off"><br><br>

                    <label>Jarak Track</label><br>
                    <input class="masukan" type="number" name="jarak" autocomplete="off"><br><br>
				
                    <label>Foto Tema</label><br>
					<input class="masukan" type="file" name="img"><br><br>

					<input class="tombol" type="submit" name="kirim" value="Tambah">
					
				</form>
			</div>
		</div>

	</div>

</body>
</html>