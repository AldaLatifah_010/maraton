<?php 

class ProgressModel{

    private $table = 'progress'; //variabel dengan sifat private, supaya variabel ini ga digunakan di tempat lain
    private $db;

    public function __construct(){
        $this->db = new Database; // koneksi ke database
    }

    public function getAllProgress(){ // function untuk ngedapatin semua data Progress
        $this->db->query('SELECT progress.*, transaksi.kode_transaksi, track.nama_track FROM ' .$this->table .
                        ' JOIN transaksi ON transaksi.id_trans = progress.id_transaksi
                          JOIN tb_detailtransaksi ON tb_detailtransaksi.id_transaksi = transaksi.id_trans
                          JOIN track ON track.id = tb_detailtransaksi.id_track
                          JOIN user ON user.id = transaksi.id_user'); //query select
        return $this->db->resultSet(); //nilai berapa baris yang diambil dari tabel Progress
    }

    public function getProgressById($id) //function untuk ngambil data Progress berdasarkan id nya
	{
		$this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=:id'); // query select berdasarkan id
		$this->db->bind('id',$id); //binding id nya
		return $this->db->single(); //ngereturn kan row single yang kita ambil
    }
    



    public function updateDataProgress($data) // function proses update data buat ke database
	{
		
        $query = "UPDATE progress SET stat=:stat, status=:status WHERE id=:id"; //ini query update
        $this->db->query($query); // jalankan querynya
        //binding data id, nama tema, deskripsi tema
        $this->db->bind('id',$data['id']); 
        $this->db->bind('stat',$data['stat']);

        if($data['stat'] == "100"){
            $this->db->bind('status', 'Selesai');
        }else{
            $this->db->bind('status','Sedang Berjalan');
        }
        
		$this->db->execute(); // run kan binding datanya

		return $this->db->rowCount(); // kita ngereturn jumlah baris yang kita update
    }
    
    public function deleteProgress($id)
	{
		$this->db->query('DELETE FROM ' . $this->table . ' WHERE id=:id');
		$this->db->bind('id',$id);
		$this->db->execute();

		return $this->db->rowCount();
    }

}