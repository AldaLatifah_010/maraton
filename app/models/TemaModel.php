<?php 

class TemaModel{

    private $table = 'tema'; //variabel dengan sifat private, supaya variabel ini ga digunakan di tempat lain
    private $db;

    public function __construct(){
        $this->db = new Database; // koneksi ke database
    }

    public function getAllTema(){ // function untuk ngedapatin semua data tema
        $this->db->query('SELECT * FROM ' .$this->table); //query select
        return $this->db->resultSet(); //nilai berapa baris yang diambil dari tabel tema
    }

    public function getTemaById($id) //function untuk ngambil data tema berdasarkan id nya
	{
		$this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=:id'); // query select berdasarkan id
        $this->db->bind('id',$id); 
        // var_dump($id);
        // die;//binding id nya
		return $this->db->single(); //ngereturn kan row single yang kita ambil
    }
    
    public function tambahTema($data){
        
        
        $ekstensi_diperbolehkan	= array('png','jpg','jpeg');
        $nama = $_FILES['img']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran	= $_FILES['img']['size'];
        $file_tmp = $_FILES['img']['tmp_name'];	

        $track = array($data);
        $track[0]['img'] = $nama;
        
        move_uploaded_file($_FILES['img']['tmp_name'], '../img/'.$nama);

        $query = "INSERT INTO tema(nama_tema, deskripsi_tema, img) VALUES (:nama_tema, :deskripsi_tema, :img)"; //isinya query tambahData Tema
        $this->db->query($query); //ngejalankan querynya
        $this->db->bind('nama_tema', $data['nama_tema']);
        $this->db->bind('deskripsi_tema', $data['deskripsi_tema']);
        $this->db->bind('img', $nama);
        $this->db->execute(); //run kan query

        return $this->db->rowCount(); //ngereturn kan jumlah baris yang ditambahkan user
    }

    public function updateDataTema($data) // function proses update data buat ke database
	{
        if ($_FILES['img']['error']===4) {
            $nama = $data['img_lama'];
        } else {
            $ekstensi_diperbolehkan	= array('png','jpg','jpeg');
            $nama = $_FILES['img']['name'];
            $x = explode('.', $nama);
            $ekstensi = strtolower(end($x));
            $ukuran	= $_FILES['img']['size'];
            $file_tmp = $_FILES['img']['tmp_name'];	

            $track = array($data);
            $track[0]['img'] = $nama;
        }

        $query = "UPDATE tema SET nama_tema=:nama_tema, deskripsi_tema=:deskripsi_tema, img=:img WHERE id=:id"; //ini query update
        $this->db->query($query); // jalankan querynya
        //binding data id, nama tema, deskripsi tema
        $this->db->bind('id',$data['id']); 
        $this->db->bind('nama_tema',$data['nama_tema']);
        $this->db->bind('deskripsi_tema',$data['deskripsi_tema']);
        $this->db->bind('img',$nama);
		
		$this->db->execute(); // run kan binding datanya

		return $this->db->rowCount(); // kita ngereturn jumlah baris yang kita update
    }
    
    public function deleteTema($id)
	{
		$this->db->query('DELETE FROM ' . $this->table . ' WHERE id=:id');
		$this->db->bind('id',$id);
		$this->db->execute();

		return $this->db->rowCount();
    }

    public function cariTema() //function cari sql buat di database
	{   
		$key = $_POST['key']; // ngepost kata kunci yang pengen kita cari
		$this->db->query("SELECT * FROM " . $this->table . " WHERE nama_tema LIKE :key"); // query select namaTema sesuai sm kata kunci yang kita cari
		$this->db->bind('key',"%$key%"); // trs kita nge binding kata kunci yang kita cari, dengan kata kunci di sql
		return $this->db->resultSet(); // kita ngereturn kan hasil dari sql kita
	}

}