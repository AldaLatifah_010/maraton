<?php 

class TransaksiModel{

    private $table = 'transaksi'; //variabel dengan sifat private, supaya variabel ini ga digunakan di tempat lain
    private $db;

    public function __construct(){
        $this->db = new Database; // koneksi ke database
    }

    public function getAllTransaksi(){
        $this->db->query("SELECT * FROM " . $this->table. " INNER JOIN user ON user.id = transaksi.id_user");
        return $this->db->resultSet();
    }

    public function getTransaksiByUser($id){
        $this->db->query("SELECT * FROM " . $this->table. " 
                            INNER JOIN user ON user.id = transaksi.id_user
                            WHERE id_user=:id");
        $this->db->bind('id_user', $id);
        return $this->db->resultSet();
    }


    public function getTransaksi(){
        $this->db->query("SELECT * FROM transaksi
                        INNER JOIN tb_detailtransaksi ON tb_detailtransaksi.id_transaksi = transaksi.id_trans
                        INNER JOIN track ON tb_detailtransaksi.id_track = track.id
                        INNER JOIN tema ON tema.id = track.id_tema
                         INNER JOIN user ON user.id = transaksi.id_user");
        return $this->db->resultSet();
    }

    public function getTransaksiById($id) //function untuk ngambil data Transaksi berdasarkan id nya
	{
		$this->db->query('SELECT * FROM ' . $this->table . ' WHERE id_trans=:id'); // query select berdasarkan id
		$this->db->bind('id',$id); //binding id nya
		return $this->db->single(); //ngereturn kan row single yang kita ambil
    }

    public function getDetailTransaksi($id){
        $this->db->query('SELECT * FROM tb_detailtransaksi
                             INNER JOIN transaksi ON tb_detailtransaksi.id_transaksi = transaksi.id_trans
                             INNER JOIN track ON tb_detailtransaksi.id_track = track.id 
                             WHERE transaksi.id_trans=:id
                              ');
        $this->db->bind('id',$id);
        return $this->db->resultSet();
    }
    
    public function tambahTransaksi($data){
        $ekstensi_diperbolehkan	= array('png','jpg','jpeg','pdf');
        $nama = $_FILES['file']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran	= $_FILES['file']['size'];
        $file_tmp = $_FILES['file']['tmp_name'];	

        $track = array($data);
        $track[0]['file'] = $nama;
        
        move_uploaded_file($_FILES['file']['tmp_name'], '../img/'.$nama);

        $query = "INSERT INTO transaksi (kode_transaksi, tgl_transaksi, file, status, id_user) VALUES (:kode_transaksi, :tgl_transaksi, :file, :status,:id_user)"; //isinya query tambahData Transaksi
        $this->db->query($query); //ngejalankan querynya
        $this->db->bind('kode_transaksi', $data['kode_transaksi']);   
        $this->db->bind('id_user', $data['id_user']);
        $this->db->bind('tgl_transaksi', date('Y-m-d'));
        $this->db->bind('file', $nama);
        $this->db->bind('status', "Belum Validasi");
        $this->db->execute();

       
        $dt = $data['id_track'];

        $id = $this->db->insert_id();
        for ($i=0; $i < count($dt) ; $i++) { 
            $query = "INSERT INTO tb_detailtransaksi(id_transaksi, id_track) VALUES (:id_transaksi, :id_track)";
            $this->db->query($query);
            $this->db->bind('id_transaksi', $id);
            $this->db->bind('id_track', $data['id_track'][$i]);
            $this->db->execute();
        }

        // $id2 = $this->db->insert_id();
        $query = "INSERT INTO progress(id_transaksi, stat, status) VALUES (:id_transaksi, :stat, :status)";

        $this->db->query($query);
        $this->db->bind('id_transaksi', $id);
		$this->db->bind('stat', 0 );
		$this->db->bind('status', 'Sedang Berjalan');
		$this->db->execute();

        return $this->db->rowCount(); //ngereturn kan jumlah baris yang ditambahkan progress
    }

    public function updateDataTransaksi($id) // function proses update data buat ke database
	{
        
        $query = "UPDATE transaksi SET status=:status WHERE id_trans=:id"; //ini query update
        $this->db->query($query); // jalankan querynya

        $this->db->bind('id_trans', $id);
        $this->db->bind('status','Tervalidasi');
		$this->db->execute(); // run kan binding datanya

		return $this->db->rowCount(); // kita ngereturn jumlah baris yang kita update
    }

    public function deleteTransaksi($id)
	{
		$this->db->query('DELETE FROM ' . $this->table . ' WHERE id=:id');
		$this->db->bind('id',$id);
		$this->db->execute();

		return $this->db->rowCount();
    }

    public function cariTransaksi() //function cari sql buat di database
	{   
		$key = $_POST['key']; // ngepost kata kunci yang pengen kita cari
		$this->db->query("SELECT * FROM " . $this->table . "
                        INNER JOIN user ON user.id = transaksi.id_user
                         WHERE kode_transaksi LIKE :key"); // query select namaTransaksi sesuai sm kata kunci yang kita cari
		$this->db->bind('key',"%$key%"); // trs kita nge binding kata kunci yang kita cari, dengan kata kunci di sql
		return $this->db->resultSet(); // kita ngereturn kan hasil dari sql kita
	}


}