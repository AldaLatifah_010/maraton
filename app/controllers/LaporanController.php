<?php 

   class laporanController extends Controller {
        public function index()
        {
            $data['title'] = 'Laporan';
            $data['level'] = $_SESSION['level'];
            $this->view('templates/header', $data); 
            $this->view('laporan/index'); 
        }
   }

?>