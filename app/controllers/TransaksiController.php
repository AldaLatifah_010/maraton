<?php

class TransaksiController extends Controller{

    public function __construct()
	{	
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/login');
			exit;
		}
	}

    public function index(){ 
        $data['title'] = "Data Transaksi"; 
        if($_SESSION['level'] == "Admin" || $_SESSION['level'] == "Pemilik"){
            $data['transaksi'] = $this->model('TransaksiModel')->getAllTransaksi();
        }else{
            $data['transaksi'] = $this->model('TransaksiModel')->getTransaksiByUser($_SESSION['id_user']);
        }
        
        // $data['y'] = $this->model('TransaksiModel')->getTrack(2);
        $data['level'] = $_SESSION['level']; 
        $this->view('templates/header', $data); 
        $this->view('transaksi/index',$data);
    }

    public function tambah(){ 
        $data['title'] = "Tambah Data Transaksi"; 
        $data['level'] = $_SESSION['level']; 
        $data['track'] = $this->model('TrackModel')->getAllTrack();
      
        $data['id_user'] = $_SESSION['id_user']; 
        $this->view('templates/header', $data);
        $this->view('transaksi/tambah', $data);
    }

    
	// public function edit($id){ 
	// 	$data['title'] = 'Edit Transaksi'; 
    //     $data['transaksi'] = $this->model('TransaksiModel')->getTranskasiById($id); 
    //     $data['level'] = $_SESSION['level']; 
    //     $this->view('templates/header', $data);
	// 	$this->view('templates/header', $data); 
	// 	$this->view('transaksi/edit', $data);
    // }

    public function cari()
	{
		$data['title'] = 'Data Transaksi';
		$data['transaksi'] = $this->model('TransaksiModel')->cariTransaksi();
		$data['key'] = $_POST['key'];
        $data['level'] = $_SESSION['level']; 
		$this->view('templates/header', $data);
		$this->view('transaksi/index', $data);
	}

    public function simpanData(){
        // var_dump($_POST);
        // die;
        if($this->model('TransaksiModel')->tambahTransaksi($_POST) > 0 ){
            
            Flasher::setMessage('Berhasil', 'ditambahkan', 'success'); 
            header('Location:'.base_url.'/transaksi' ); 
            exit;
        }else{
            Flasher::setMessage('Gagal', 'ditambahkan', 'danger'); 
            header('Location:'.base_url.'/transaksi' ); 
            exit;
        }
    }

    public function edit($id){	
        if( $this->model('TransaksiModel')->updateDataTransaksi($id) > 0) {
            Flasher::setMessage('Berhasil','diupdate','success');
            header('Location: '. base_url . '/transaksi');
            exit;			
        }else{
            Flasher::setMessage('Gagal','diupdate','danger');
            header('Location: '. base_url . '/transaksi');
            exit;	
        }
    }

    public function print_laporan(){
        $data['title'] = "DATA LAPORAN";
        $data['transaksi'] = $this->model('TransaksiModel')->getTransaksi();
        $this->view('laporan/index', $data);
    }

    public function hapus($id){
		if( $this->model('TransaksiModel')->deleteTransaksi($id) > 0 ) {
			Flasher::setMessage('Berhasil','dihapus','success');
			header('Location: '. base_url . '/transaksi');
			exit;			
		}else{
			Flasher::setMessage('Gagal','dihapus','danger');
			header('Location: '. base_url . '/transaksi');
			exit;	
		}
	}

}