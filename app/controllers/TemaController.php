<?php

class TemaController extends Controller{

    public function __construct()
	{	
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/login');
			exit;
		}
	}

    public function index(){ //function tampil data
        $data['title'] = "Data Tema"; // ini untuk title di desainnya
        $data['tema'] = $this->model('TemaModel')->getAllTema();
        $data['level'] = $_SESSION['level']; //ngambil function getAllTema dari TemaModel
        //ngereturn/nampilin header,index, footer,sidebar saat kita ngejalankan fungsi index
        $this->view('templates/header', $data); 
        $this->view('tema/index',$data);
    }

    public function tambah(){ //function tampil halaman tambah
        $data['title'] = "Tambah Data Tema"; //title untuk desainnya
        $data['tema'] = $this->model('TemaModel')->getAllTema(); //ngambil function getAllTema dari TemaModel
        $data['level'] = $_SESSION['level']; 
        // $data['kode'] = $this->model('TemaModel')->getKode();
        // // $rin = $data['kode'];
        // if ($$data['kode']) {
        //     $kode = '';
        //     $r    = substr($data['kode'], 2,4);
        //     $n    = (int)$r+1;
        //     if (strlen($n)==1) {
        //         $kode = 'RN00'.$n.$tanggal;
        //     } else if (strlen($n)==2){
        //         $kode = 'RN0'.$n.$tanggal;
        //     } else {
        //         $kode = 'RN'.$n.$tanggal;
        //     }
        // }
        // var_dump($data['kode']);
        // die;

        //ngereturn header, tambahTema, sidebar, footer saat jalannya fungsi tambah
        $this->view('templates/header', $data);
        $this->view('tema/tambah', $data);
    }

    
	public function edit($id){ //function halaman edit
		$data['title'] = 'Edit Tema'; //title untuk desain halamannya
        $data['tema'] = $this->model('TemaModel')->getTemaById($id); // ngambil function getTemaById dari model
        $data['level'] = $_SESSION['level']; 
        //ngereturn header, tambahTema, sidebar, footer saat jalannya fungsi tambah
        $this->view('templates/header', $data);
		$this->view('tema/edit', $data);
    }

    public function cari()
	{
		$data['title'] = 'Data Tema';
		$data['tema'] = $this->model('TemaModel')->cariTema();
        $data['level'] = $_SESSION['level']; 
		$data['key'] = $_POST['key'];
		$this->view('templates/header', $data);
		$this->view('tema/index', $data);
		
	}

    public function simpanData(){
        if($this->model('TemaModel')->tambahTema($_POST) > 0 ){
            Flasher::setMessage('Berhasil', 'ditambahkan', 'success'); //notifikasi
            header('Location:'.base_url.'/tema' ); //redirect ke halaman localhost/marathon/public/tema
            exit;
        }else{
            Flasher::setMessage('Gagal', 'ditambahkan', 'danger'); //notifikasi
            header('Location:'.base_url.'/tema' ); //redirect ke halaman
            exit;
        }
    }

    public function updateTema(){	

        if( $this->model('TemaModel')->updateDataTema($_POST) > 0 ) {
            Flasher::setMessage('Berhasil','diupdate','success');
            header('Location: '. base_url . '/tema');
            exit;			
        }else{
            Flasher::setMessage('Gagal','diupdate','danger');
            header('Location: '. base_url . '/tema');
            exit;	
        }
    }

    public function hapus($id){
		if( $this->model('TemaModel')->deleteTema($id) > 0 ) {
			Flasher::setMessage('Berhasil','dihapus','success');
			header('Location: '. base_url . '/tema');
			exit;			
		}else{
			Flasher::setMessage('Gagal','dihapus','danger');
			header('Location: '. base_url . '/tema');
			exit;	
		}
	}

}