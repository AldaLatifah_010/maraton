<?php

class LoginController extends Controller {
	
	public function index()
	{
		$this->view('login/login'); 
	}

	public function register()
	{
		$data['title'] = 'Halaman Register';
		$this->view('login/register', $data);
	}

	public function prosesLogin() {
		
		if($this->model('LoginModel')->checkLogin($_POST) > 0 ) {
				$row = $this->model('LoginModel')->checkLogin($_POST);
				
				$_SESSION['username'] = $row["username"];
				$_SESSION['nama'] = $row["nama"];
				$_SESSION['level'] = $row["level"];
				$_SESSION['id_user'] = $row['id'];
				$_SESSION['session_login'] = 'sudah_login';
				
				header('Location: '. base_url . '/home');
		} else {
			Flasher::setMessage('Username / Password','salah.','danger');
			header('Location: '. base_url . '/login');
			exit;	
		}
	}

	public function tambahRegister(){
		if($this->model('LoginModel')->tambahUser($_POST) > 0 ){
			Flasher::setMessage('Berhasil', 'ditambahkan', 'success'); 
			header('Location:'.base_url.'/login' ); 
			exit;
		}else{
			Flasher::setMessage('Gagal', 'ditambahkan', 'danger'); 
			header('Location:'.base_url.'/login' ); 
			exit;
		}
	}

    public function logout(){
		session_start();
		session_destroy();
		header('Location: '. base_url . '');
	}


}