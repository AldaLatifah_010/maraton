<?php 

    class TrackController extends Controller{

        public function __construct()
        {	
            if($_SESSION['session_login'] != 'sudah_login') {
                Flasher::setMessage('Login','Tidak ditemukan.','danger');
                header('location: '. base_url . '/login');
                exit;
            }
        }

        public function index()
        {
            $data['title'] = 'Data Track';
            
            $data['track'] = $this->model('TrackModel')->getAllTrack();
            $data['level'] = $_SESSION['level'];
            $this->view('templates/header', $data);
            $this->view('track/index', $data);
        }

        
        public function tambah(){ 
            $data['title'] = "Tambah Data Track"; 
            $data['tema'] = $this->model('TemaModel')->getAllTema(); 
            $data['level'] = $_SESSION['level']; 
            $this->view('templates/header', $data);
            $this->view('track/tambah', $data);
        }

        public function edit($id){ //function halaman edit
            $data['title'] = 'Edit Track'; //title untuk desain halamannya
            $data['track'] = $this->model('TrackModel')->getTrackById($id);
            $data['tema'] = $this->model('TemaModel')->getAllTema(); // ngambil function getTemaById dari model
            $data['level'] = $_SESSION['level']; 
            //ngereturn header, tambahTema, sidebar, footer saat jalannya fungsi tambah
            $this->view('templates/header', $data);
            $this->view('track/edit', $data);
        }

        public function simpanData(){
            if($this->model('TrackModel')->tambahTrack($_POST) > 0 ){
                Flasher::setMessage('Berhasil', 'ditambahkan', 'success'); 
                header('Location:'.base_url.'/track' ); 
                exit;
            }else{
                Flasher::setMessage('Gagal', 'ditambahkan', 'danger'); 
                header('Location:'.base_url.'/track' ); 
                exit;
            }
        }

        public function updateTrack(){	

            if( $this->model('TrackModel')->updateDataTrack($_POST) > 0 ) {
                Flasher::setMessage('Berhasil','diupdate','success');
                header('Location: '. base_url . '/track');
                exit;			
            }else{
                Flasher::setMessage('Gagal','diupdate','danger');
                header('Location: '. base_url . '/track');
                exit;	
            }
        }

        public function hapus($id){
            if( $this->model('TrackModel')->deleteTrack($id) > 0 ) {
                Flasher::setMessage('Berhasil','dihapus','success');
                header('Location: '. base_url . '/track');
                exit;			
            }else{
                Flasher::setMessage('Gagal','dihapus','danger');
                header('Location: '. base_url . '/track');
                exit;	
            }
        }

    }