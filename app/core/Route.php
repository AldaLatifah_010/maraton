<?php 

    class Route{

        protected $params = [];


        public function get(string $routename, $callback)
        {
          if ($this->get_uri() == $routename) {
              if (isset($_GET)) {
                if (is_string($callback)) {
                    echo $this->string_handler($callback);
                }else{
                    $callback($callback);
                }
              }
          }
        }

        public function post(string $routename, $callback)
        {
          if ($this->get_uri() == $routename) {
              if (isset($_POST)) {
                if (is_string($callback)) {
                    echo $this->string_handler($callback);
                }else{
                    $callback($callback);
                }
              }
          }
        }

        public function edit(string $routename, $callback){
            if ($this->get_uri() == $routename) {

                $exp = explode("@", $callback);
                $className = $exp[0];

                require "../app/controllers/$className.php";
                $class = new $className;

                return $class->edit($this->get_uri_id());
            }
        }

        public function hapus(string $routename, $callback){
            if ($this->get_uri() == $routename) {

                $exp = explode("@", $callback);
                $className = $exp[0];
                

                require "../app/controllers/$className.php";
                $class = new $className;

                return $class->hapus($this->get_uri_id());
            }
        }


        public function parseURL(){
            if( isset($_GET['url'])){
                $url = rtrim($_GET['url'], '/');
                $url = filter_var($url, FILTER_SANITIZE_URL);
                $url = explode('/',$url);
                return $url;
            }
            
        }

        public function string_handler(string $string)
        {
            if (strpos($string,"@")) {
                return $this->class_handler($string);
            }else{
                return $string;
            }
        }

        public function string_handler_edit(string $string)
        {
            if (strpos($string,"@")) {
                return $this->class_handler_edit($string);
            }else{
                return $string;
            }
        }

        public function class_handler($string)
        {
            $exp = explode("@", $string);
            $className = $exp[0];
            $functionName = $exp[1];
            require "../app/controllers/$className.php";
            $class = new $className;
            return $class->$functionName();
        }

        public function class_handler_edit($string)
        {
            $exp = explode("@", $string);
            $className = $exp[0];
            $functionName = $exp[1];
            require "../app/controllers/$className.php";
            $class = new $className;

            return $class->$functionName($this->get_uri_id());
        }

        public function get_uri(){
            $uri = $_SERVER['REQUEST_URI']; //post get  http://localhost/marathon/public/login post atau get
            $exp = explode("/", $uri); // $exp = [localhost, marathon, public, login,] 
            $uri = $uri[2]; //$uri public 
            if($uri = "maraton"){
                $uri = $exp[3]; //public  
            }

            // http://localhost/marathon/public/login?id=

            //fungsi substr kita ngambil string ke empat aja  
            $uri = (strpos($uri, "?")) ? substr($uri, $exp[4], strpos($uri, "?")) : $uri;

            return $uri; 
        }


        public function get_uri_id(){
            $uri = $_SERVER['REQUEST_URI'];
            $exp = explode("/", $uri);
            $uri = $uri[2];
            if($uri = "maraton"){
                $uri = $exp[4];
            }
            $uri = (strpos($uri, "?")) ? substr($uri, $exp[5], strpos($uri, "?")) : $uri;
            $int = (int)$uri;

            return $int;
        }
    }


?>