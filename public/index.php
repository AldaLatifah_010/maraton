<?php

if( !session_id() ) session_start();

require_once '../app/init.php';

$route = new Route; //

//route tampil halaman 
//route(get_uri) -> index(ngambil route name yang == get_uri) -> route(ngejalankan fungsi get)
//fungsi get-> ngepecah ProfilController@index yang ada di fungsi string handler
//data string handler = profilController, index -> ke fungsi class handler
//di class handler -> controller (profilController) -> method (index)
$route->get('', 'ProfilController@index');
$route->get('login', 'LoginController@index');
$route->get('register', 'LoginController@register');

//tampil halaman melalui login
$route->get('home', 'HomeController@index');
$route->get('tema', 'TemaController@index');
$route->get('track', 'TrackController@index');
$route->get('progress', 'ProgressController@index');
$route->get('transaksi', 'TransaksiController@index');
$route->get('user', 'UserController@index');
$route->get('laporan', 'TransaksiController@print_laporan');
$route->get('logout', 'LoginController@logout');

// //tampil halaman form tambah
$route->get('tambahTema', 'TemaController@tambah');
$route->get('tambahTrack', 'TrackController@tambah');
$route->get('tambahTransaksi', 'TransaksiController@tambah');
$route->get('tambahUser', 'UserController@tambah');

// //tampil halaman form edit
$route->edit('temaEdit','TemaController@edit');
$route->edit('trackEdit', 'TrackController@edit');
$route->edit('progressEdit', 'ProgressController@edit');
// $route->edit('transaksiEdit', 'TransaksiController@edit');
$route->edit('editTransaksi', 'TransaksiController@edit');
$route->edit('userEdit', 'UserController@edit');


//buat nampilin data atau halaman atau method get, alurnya dari route kemudian ke controller baru ke method/function
//setelah itu ke view 

// //route post data
$route->post('prosesLogin','LoginController@prosesLogin');
$route->post('registrasi','LoginController@tambahRegister');
$route->post('simpanTema','TemaController@simpanData');
$route->post('simpanTrack','TrackController@simpanData');
$route->post('simpanProgress','ProgressController@simpanData');
$route->post('simpanTransaksi','TransaksiController@simpanData');
$route->post('simpanUser','UserController@simpanData');
$route->post('registrasi','LoginController@register');
$route->post('logout', 'LoginController@logout');
$route->post('cariTransaksi', 'TransaksiController@cari');

//route edit data
$route->post('editTema', 'TemaController@updateTema');
$route->post('editTrack', 'TrackController@updateTrack');
$route->post('editProgress', 'ProgressController@updateProgress');

$route->post('editUser', 'UserController@updateUser');

//route hapus data
$route->hapus('hapusTrack', 'TrackController@hapus');
$route->hapus('hapusTema', 'TemaController@hapus');
$route->hapus('hapusProgress', 'ProgressController@hapus');
$route->hapus('hapusTransaksi', 'TransaksiController@hapus');
$route->hapus('hapusUser', 'UserController@hapus');





